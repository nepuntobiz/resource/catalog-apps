# GENERAL
{{- define "jitsi.name" -}}
{{- printf "jitsi" -}}
{{- end -}}

{{- define "jitsi.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

# FOCUS
{{- define "jicofo.name" -}}
{{- printf "jicofo" -}}
{{- end -}}

{{- define "jicofo.component" -}}
{{- printf "focus" -}}
{{- end -}}

# PROSODY
{{- define "prosody.name" -}}
{{- printf "prosody" -}}
{{- end -}}

{{- define "prosody.component" -}}
{{- printf "xmpp" -}}
{{- end -}}

{{- define "prosody.xmpp" -}}
{{- printf "%s.%s" (include "prosody.name" .) .Release.Namespace -}}
{{- end -}}

# VIDEO BRIDGE
{{- define "jvb.name" -}}
{{- printf "jvb" -}}
{{- end -}}

{{- define "jvb.component" -}}
{{- printf "sfu" -}}
{{- end -}}

# MEET
{{- define "meet.name" -}}
{{- printf "meet" -}}
{{- end -}}

{{- define "meet.component" -}}
{{- printf "web" -}}
{{- end -}}

