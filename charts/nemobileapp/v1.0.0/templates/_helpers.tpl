{{- define "nemobileapp.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "keyencode" -}}
{{ printf "APP_KEY:%s" (randAlpha 24) | b64enc }}
{{- end -}}

{{- define "keygenerate" -}}
{{ printf "base64:%s" (include "keyencode" .) }}
{{- end -}}

{{- define "application.type" -}}
{{- if  eq .Values.client.type "realestate" }}
{{- printf "re" -}}
{{- else if eq .Values.client.type "elearning" }}
{{- printf "el" -}}
{{- end }}
{{- end -}}